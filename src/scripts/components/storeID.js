export const storeID = (storageParam, id, location) => {
	if (typeof storageParam !== 'string' && typeof id !== 'string' && typeof location !== 'string') {
		return null;
	}
	if (location.includes('.html') === false && location.endsWith('.html') === false) {
		return null;
	}
	sessionStorage.setItem(storageParam, id);
	window.location = location;
};