import axios from 'axios';

export const fetchMovieInfo = (apiParam, apikey) => {
	if (typeof apikey !== 'string') {
		return null;
	}
	if (typeof apiParam !== 'string') {
		return null;
	}
	if (apiParam.length !== 1) {
		return null;
	}
	const movieId = sessionStorage.getItem('movieId');
	axios.get(`http://www.omdbapi.com/?${apiParam}=${movieId}&apikey=${apikey}`)
		.then(response => {
			console.log(response);
			let movieItem = response.data;

			let movieTitle = movieItem.Title;
			document.getElementById('movieTitle').innerText = movieTitle;

			let movieImage = movieItem.Poster;
			document.getElementById('imagePoster').src = movieImage;
			document.getElementById('imagePoster').setAttribute('alt', movieTitle.toLowerCase().replace(' ', '-'));

			let moviePlot = movieItem.Plot;
			document.getElementById('moviePlot').innerText = moviePlot;

			let movieActors = movieItem.Actors;
			document.getElementById('movieActors').innerText = movieActors;

			let movieDirector = movieItem.Director;
			document.getElementById('movieDirector').innerText = movieDirector;

			let movieBoxOffice = movieItem.BoxOffice;
			document.getElementById('movieBoxOffice').innerText = movieBoxOffice;

			let movieGenre = movieItem.Genre;
			document.getElementById('movieGenre').innerText = movieGenre;

			let movieimdbRating = movieItem.imdbRating;
			document.getElementById('movieimdbRating').innerText = movieimdbRating;

			let imdbLink = document.getElementById('imdbLink');
			imdbLink.setAttribute('href', `https://www.imdb.com/title/${movieId}`);
		})
		.catch(error => console.log(error));
};