export const removeNodes = (id) => {
	let element = document.getElementById(id);
	while (element.firstChild) {
		element.removeChild(element.firstChild);
	}
};