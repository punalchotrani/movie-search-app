import '../styles/index.scss';

//Import components
import { getMovies } from '../scripts/components/getMovies';
import { removeNodes } from './components/removeNodes';

const searchForm = document.getElementById('searchForm');
searchForm.addEventListener('keyup', () => {
	let searchValue = document.getElementById('searchInput').value;
	getMovies('S', searchValue, '190ab504');
	removeNodes('movieListWrapper');
});