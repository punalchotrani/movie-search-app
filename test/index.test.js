import { storeID } from '../src/scripts/components/storeID';
import 'jest-localstorage-mock';
import { getMovies } from '../src/scripts/components/getMovies';
import { fetchMovieInfo } from '../src/scripts/components/fetchMovieInfo';

describe('Check the functionality of storeID', () => {
	it('Should check for parameters', () => {
		expect(storeID(1, 2, 3)).toBeNull();
		expect(storeID({}, {}, {})).toBeNull();
		expect(storeID([], [], [])).toBeNull();
	});
});

describe('Check the 3rd Argument for storeID function', () => {
	it('Should check for string value', () => {
		expect(storeID('String1', 'String2', 'about.html')).not.toBeNull();
	});
	it('Should check for string position', () => {
		expect(storeID('String1', 'String2', 'html.about')).toBeNull();
		expect(storeID('String1', 'String2', 'about.html')).not.toBeNull();
	});
});

describe('Arguments passed into function', () => {
	it('It should check for argument type', () => {
		expect(getMovies(9, () => { }, [])).toBeNull();
		expect(getMovies('s', 'String2', 'String3')).not.toBeNull();
	});
});

describe('Check the 1st Argument for getMovies function', () => {
	it('Should check the argument length', () => {
		expect(getMovies('S', 'String2', 'String3')).not.toBeNull();
	});
});

describe('Check the 1st Argument for fetchMovieInfo function', () => {
	it('Should check the argument length & type', () => {
		expect(fetchMovieInfo('i', '2958259')).not.toBeNull();
		expect(fetchMovieInfo('I', '4728756')).not.toBeNull();
		expect(fetchMovieInfo('i', 23948)).toBeNull();
		expect(fetchMovieInfo(295, 23948)).toBeNull();
		expect(fetchMovieInfo([], [])).toBeNull();
		expect(fetchMovieInfo({}, {})).toBeNull();
		expect(fetchMovieInfo('rt', {})).toBeNull();
	});
});