import axios from 'axios';
import { storeID } from './storeID';

export let getMovies = (apiParam, searchMovie, apiKey) => {
	if (typeof apiParam !== 'string' && typeof searchMovie !== 'string' && typeof apiKey !== 'string') {
		return null;
	}
	if (apiParam.length !== 1) {
		return null;
	}
	axios
		.get(`http://www.omdbapi.com/?${apiParam}=${searchMovie}&apikey=${apiKey}`)
		.then((response) => {
			let movies = response.data.Search;
			console.log(movies);
			let ul = document.createElement('ul');
			ul.className = "collection";
			document.getElementById('movieListWrapper').appendChild(ul);
			movies.forEach(movie => {
				let li = document.createElement('li');
				li.className = "collection-item avatar show";
				ul.appendChild(li);
				li.innerHTML += `
					<a class="details" href="#" data-movieid="${movie.imdbID}">
						<div>
							<img src="${movie.Poster}" alt="${movie.Title}" class="circle">
							<span class="title">${movie.Title}</span>
						</div>
					</a>
				`;

				let details = document.querySelectorAll('ul:last-child .details');
				details.forEach(detail => {
					detail.addEventListener('click', e => {
						e.preventDefault();
						storeID('movieId', detail.dataset.movieid, 'detail.html');
					});
				});
			});
		})
		.catch((error) => console.log('No movie available'));
};